package Assignment3.domain;

public class User {
    private int id;
    private int id_gen=0;
    private String name;
    private String surname;
    private String username;
    private Password password;

    public User(int id, String name, String surname, String username) {
    setId(id);
    setName(name);
    setSurname(surname);
    setUsername(username);
    }
    public User(int id, String name, String surname, String username, Password password){
        this(id, name, surname , username);
        setPassword(password);
    }

    private void generateId(){
        id=id_gen++;
    }
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public Password getPassword() {
        return password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + surname + " " + username + " " + password;
    }
}
