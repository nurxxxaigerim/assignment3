package Assignment3.domain;

public class Password {
    // passwordStr // it should contain uppercase and lowercase letters and digits
// and its length must be more than 9 symbols
    private String passwordStr;

    public Password(String passwordStr) {
        setPasswordStr(passwordStr);
    }

    public String getPasswordStr() {
        return passwordStr;
    }

    public void setPasswordStr(String passwordStr) {
        this.passwordStr = passwordStr;
    }

    public static boolean passVal(String passwordStr) {
        if (passwordStr() > 9) {
            if (checkPasswordFormat(passwordStr)) {
                return true;
            }
            return false;
        } else {
            System.out.println("This password format is incorrect!");
            return false;
        }
    }

    public static boolean checkPasswordFormat(String passwordStr) {
        boolean hasNum = false;
        boolean hasCap = false;
        boolean hasLow = false;
        char c;
        for (int i = 0; i < passwordStr.length(); i++) {
            c = passwordStr.charAt(i);
            if (Character.isDigit(c)) {
                hasNum = true;
            } else if (Character.isUpperCase(c)) {
                hasCap = true;
            } else if (Character.isLowerCase(c)) {
                hasLow = true;
            }
            if (hasNum && hasCap && hasLow) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return
    }
}