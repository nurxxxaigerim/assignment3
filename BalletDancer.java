package Assignment3.domain;

public class BalletDancer extends User {
    private User name;
    private User surname;
    private User username;
    private Password password;
    private double weight;
    private double height;

    public BalletDancer(User name,User surname,User username, Password password){
        super(name, "Gaukhar", "Nurlanova", surname);
    }
    public BalletDancer(User name,User surname,User username, Password password, double weight, double height){
        this(name, surname, username, password);
        setWeight(weight);
        setHeight(height);
    }
    @Override
    public User getName() {
        return name;
    }

    @Override
    public User getSurname() {
        return surname;
    }

    @Override
    public User getUsername() {
        return username;
    }

    @Override
    public Password getPassword() {
        return password;
    }

    @Override
    public void setPassword(Password password) {
        this.password = password;
    }

    public void setName(User name) {
        this.name = name;
    }

    public void setSurname(User surname) {
        this.surname = surname;
    }

    public void setUsername(User username) {
        this.username = username;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return super.toString() + "information about ballet dancer." + " Her weight is " + weight + ". And height is "
                + height;
    }
}